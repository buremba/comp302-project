import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

class ApplicationWindow extends JFrame {
    // overview: An ApplicationWindow is a top level program window that
    // contains a toolbar and an animation window.
    protected AnimationWindow animationWindow;

    public ApplicationWindow() {
        // effects: Initializes the application window so that it contains
        //          a toolbar and an animation window.
        // Title bar

        super("Swing Demonstration Program");
        // respond to the window system asking us to quit
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        //Create the toolbar.
        JToolBar toolBar = new JToolBar();
        addButtons(toolBar);
        //Create the animation area used for output.
        animationWindow = new AnimationWindow();
        // Put it in a scrollPane, (this makes a border)
        JScrollPane scrollPane = new JScrollPane(animationWindow);
        //Lay out the content pane.
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setPreferredSize(new Dimension(Wall.width+4, Wall.px+26));
        contentPane.add(toolBar, BorderLayout.NORTH);
        contentPane.add(scrollPane, BorderLayout.CENTER);
        setContentPane(contentPane);
    }
    protected void addButtons(final JToolBar toolBar) {
        final JFrame frame = this;
        final JButton runButton = new JButton("Run");

        runButton.setToolTipText("Start the animation");
        // when this button is pushed it calls animationWindow.setMode(true)
        runButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                animationWindow.setMode(1);
            }
        });
        toolBar.add(runButton);
        final JButton stopButton = new JButton("Stop");
        stopButton.setToolTipText("Stop the animation");
        // when this button is pushed it calls animationWindow.setMode(false)
        stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                animationWindow.setMode(0);
            }
        });
        toolBar.add(stopButton);
        final JButton quitButton = new JButton("Quit");
        quitButton.setToolTipText("Quit the program");
        quitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        toolBar.add(quitButton);

        final JButton editButton = new JButton("Edit Mode");
        editButton.setToolTipText("Delete the scene and switch to Edit Mode");
        editButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                animationWindow.setMode(2);
                // The edit part doesn't remove all gizmos because otherwise its name would be "new game". I created another button called "remove gizmos" for this function
                // animationWindow.ball.gizmos.clear();
                toolBar.remove(runButton);
                toolBar.remove(stopButton);
                toolBar.remove(quitButton);
                toolBar.remove(editButton);
                final JButton addGizmoButton = new JButton("Add Gizmo");
                addGizmoButton.setToolTipText("Add new Gizmo to the scene");
                addGizmoButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JDialog d = new AddGizmoDialog(frame, true, animationWindow);
                        d.setLocationRelativeTo(frame);
                        d.setVisible(true);
                    }
                });
                toolBar.add(addGizmoButton);
                final JButton removeAllGizmosButton = new JButton("Remove Gizmos");
                removeAllGizmosButton.setToolTipText("Remove All Gizmos in the Scene");
                removeAllGizmosButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        animationWindow.gizmos.clear();
                        repaint();
                    }
                });
                toolBar.add(removeAllGizmosButton);
                final JButton saveXMLButton = new JButton("Save Scene");
                saveXMLButton.setToolTipText("Save Scene to an XML file");
                saveXMLButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JDialog d = new SaveScenetoXMLDialog(frame, true, animationWindow);
                        d.setLocationRelativeTo(frame);
                        d.setVisible(true);
                    }
                });
                toolBar.add(saveXMLButton);
                final JButton runGameButton = new JButton("Run Game");
                runGameButton.setToolTipText("Run game with this scene");
                runGameButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        animationWindow.setMode(1);
                        toolBar.add(runButton);
                        toolBar.add(stopButton);
                        toolBar.add(quitButton);
                        toolBar.add(editButton);

                        toolBar.remove(addGizmoButton);
                        toolBar.remove(runGameButton);
                        toolBar.remove(removeAllGizmosButton);
                        toolBar.remove(saveXMLButton);
                        toolBar.repaint();
                    }
                });
                toolBar.add(runGameButton);
                toolBar.revalidate();
                toolBar.repaint();
                animationWindow.addMouseListener(new MouseAdapter(){
                    public void mouseClicked (MouseEvent e)
                    {
                        final Point p = e.getPoint();
                        final Gizmo g = animationWindow.getGizmoFromPoint(p);
                        if(g!=null) {
                            JDialog d = new EditGizmoDialog(frame, true, animationWindow, g);
                            d.setLocationRelativeTo(frame);
                            d.setVisible(true);
                        }
                    }
                });
                repaint();
            }
        });
        toolBar.add(editButton);
    }
}
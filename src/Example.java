import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Example {

    public static void main(String[] args) throws SAXException, ParserConfigurationException {

        Source schemaFile = new StreamSource(new File("cezmiball.xsd"));

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(schemaFile);
        Validator validator = schema.newValidator();

        String filename = null;
        try {
            /*
            System.out.print("Please enter XML file for Map: ");
            Scanner sc= new Scanner(System.in);
            filename = sc.nextLine(); */
            filename = "cezmi.xml";
            Source xmlFile = new StreamSource(new File(filename));
            validator.validate(xmlFile);
        } catch (Exception e) {
            System.out.println("Error while validating XML file: ");
            System.out.println(e.getMessage());
        }


        ArrayList<Gizmo> gizmos = new ArrayList<Gizmo>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(filename);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("squareBumper");
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    int x = Integer.parseInt(eElement.getAttribute("x"));
                    int y = Integer.parseInt(eElement.getAttribute("y"));

                    if(0<=x && x<=20 && 0<=y && y<=20) {
                        gizmos.add(new SquareTakoz(x, y));
                    }


                }


            }
            gizmos.add(new Tokat(12, 10));
            gizmos.add(new TriangleTakoz(16, 10));
            gizmos.add(new TriangleTakoz(7, 10));

            NodeList balltag = doc.getElementsByTagName("ball");
            Ball ball;
            if (balltag.getLength()>0) {
                Element ballnode = (Element) balltag.item(0);
                ball = new Ball(40);
            }else {
                ball = new Ball(40);
            }

            ApplicationWindow frame = new ApplicationWindow();
            frame.pack();
            frame.animationWindow.gizmos = gizmos;
            frame.animationWindow.ball.setBall(ball);
            frame.setVisible(true);
        } catch (IOException e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }

    }
}
import physics.*;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Created by buremba on 29/12/13.
 */
public class Tokat extends Gizmo {
    private Boolean opening = false;
    final private boolean side;

    final private LineSegment s_top;
    final private LineSegment s_right;
    final private LineSegment s_bottom;
    final private LineSegment s_left;

    Angle angle = new Angle(0);
    private Boolean rotation = null;

    public Tokat(int x, int y) {
        super(Color.BLUE, x, y);
        side = x>10;

        s_top = new LineSegment(x, y, x + width, y);
        s_right = new LineSegment(x + width, y, x + width, y + 4);
        s_bottom = new LineSegment(x, y + 4, x + width, y + 4);
        s_left = new LineSegment(x, y, x, y + 4);


    }

    public void collide(Ball ball) {
        Circle c = ball.getCircle();
        Vect v = ball.v;


        LineSegment bottom = new LineSegment(x, y, x + width, y);
        LineSegment dynamic = new LineSegment(x, y, (float) (x+(angle.sin()*width)), (float) (y+(angle.cos()*height)));

        if(Geometry.timeUntilWallCollision(bottom, c, v)<=2)
            ball.v = Geometry.reflectWall(bottom, v);
        if(Geometry.timeUntilWallCollision(dynamic, c, v)<=2)
            ball.v = Geometry.reflectWall(dynamic, v);
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(4));
        g2.setColor(color);

        g2.draw(new Line2D.Float(x, y, x + width, y));
        if (!side)
            g2.draw(new Line2D.Float(x, y, (float) (x+(angle.sin()*width)), (float) (y+(angle.cos()*height))));
        else
            g2.draw(new Line2D.Float(x+width, y, (float) (x+width-(angle.sin()*width)), (float) (y+(angle.cos()*height))));
    }
    public void clock() {
        if (rotation != null)
        if(rotation==opening) {
            angle = new Angle(angle.radians()+0.1);
            if (angle.radians()>1.6)
                rotation = null;
        }
        else
        if(!rotation==opening) {
            angle = new Angle(angle.radians()-0.1);
            if (angle.radians()<0)
                rotation = null;
        }
    }

    public void toggle() {
        opening = !opening;
        rotation = true;
    }
}

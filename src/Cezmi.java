import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.Vect;

import java.awt.*;
import java.util.ArrayList;

public class Cezmi {
    public int score = 0;
    public float x;
    public final static int radius = 10;
    public final static int y = Wall.px-(radius/4);


    public Cezmi(float x, int score) {
        this.x = x;
        this.score = score;
    }

    public Cezmi(float x) {
        this.x = x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void paint(Graphics g) {
        g.setColor(Color.darkGray);
        g.fillOval((int) x-radius, y-radius, radius*2, radius*2);
        g.setColor(Color.green);
        g.fillOval((int) x-radius+3, y-radius+2, 4, 4);
    }

    public void incrementX() {
        this.x += 5;
    }

    public Vect getPosition() {
        return new Vect(new Angle(x, y), radius);
    }

    public Circle getCircle() {
        return new Circle(x, y, radius);
    }

    public void collide(Ball ball) {
        if(Geometry.timeUntilCircleCollision(getCircle(), ball.getCircle(), ball.v)<=0)
            ball.v = Geometry.reflectCircle(getPosition(), ball.getPositionWithLength(), ball.v);
    }

    public void decrementX() {
        this.x -= 5;
    }
}

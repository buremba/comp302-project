import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

/**
 * Created by buremba on 05/01/14.
 */
public class Wall {
    final static int px = 500;
    final static int width = 400;

    final static LineSegment top = new LineSegment(0, 0, width, 0);
    final static LineSegment right = new LineSegment(width, 0, width, px);
    final static LineSegment bottom = new LineSegment(0, px, width, px);
    final static LineSegment left = new LineSegment(0, 0, 0, px);

    public final static LineSegment collide(Ball ball) {
        Circle c = ball.getCircle();
        Vect v = ball.v;
        if(Geometry.timeUntilWallCollision(top, c, v)<=0) {
            ball.v = Geometry.reflectWall(top, ball.v);
            return top;
        }
        else if (Geometry.timeUntilWallCollision(right, c, v)<=0) {
            ball.v = Geometry.reflectWall(right, ball.v);
            return right;
        }
        else if (Geometry.timeUntilWallCollision(bottom, c, v)<=0) {
            ball.v = Geometry.reflectWall(bottom, ball.v);
            return bottom;
        }
        else if (Geometry.timeUntilWallCollision(left, c, v)<=0) {
            ball.v = Geometry.reflectWall(left, ball.v);
            return left;
        }
        return null;
    }
}

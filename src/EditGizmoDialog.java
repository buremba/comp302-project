import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class EditGizmoDialog extends JDialog {
    JLabel x = new JLabel("X");

    JLabel y = new JLabel("Y");

    JButton createButton = new JButton("Edit");
    JButton removeButton = new JButton("Remove");


    JTextField xInput = new JTextField();

    JTextField yInput = new JTextField();

    public EditGizmoDialog(Frame owner, boolean modal, AnimationWindow animationWindow, Gizmo g) {
        super(owner, modal);
        init(animationWindow, owner, g);
    }
    private void init(final AnimationWindow animationWindow, final Frame owner, final Gizmo g) {
        this.setTitle("Edit Gizmo");
        this.setLayout(new GridLayout(4, 2));
        xInput.setText(String.valueOf(g.x/20));
        yInput.setText(String.valueOf(g.y/20));
        this.add(x);
        this.add(xInput);
        this.add(y);
        this.add(yInput);
        removeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                animationWindow.gizmos.remove(g);
                owner.repaint();
                closeDialog();
            }}
        );

        createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Integer x = null;
                Integer y = null;
                try {
                    x = Integer.parseInt(xInput.getText());
                    y = Integer.parseInt(yInput.getText());
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, "X and Y must be integers.");
                    return;
                }

                if (x<=20 && x>=0 && y <= 20 && y>=0) {
                    g.x = x*20;
                    g.y = y*20;
                    owner.repaint();
                    closeDialog();
                } else
                    JOptionPane.showMessageDialog(null, "X and Y must be between 0 and 20.");
            }
        });
        this.add(createButton);
        this.add(removeButton);
    }
    void closeDialog(){
        setModal(false);
        this.dispose();
    }
}
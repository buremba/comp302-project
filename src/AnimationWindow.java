import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

// Note the very indirect way control flow works during an animation:
//
// (1) We set up an eventListener with a reference to the animationWindow.
// (2) We set up a timer with a reference to the eventListener.
// (3) We call timer.start().
// (4) Every 20 milliseconds the timer calls eventListener.actionPerformed()
// (5) eventListener.actionPerformed() modifies the logical
//     datastructure (e.g. changes the coordinates of the ball).
// (6) eventListener.actionPerformed() calls myWindow.repaint.
// (7) Swing schedules, at some point in the future, a call to
//      myWindow.paint()
// (8) myWindow.paint() tells various objects to paint
//     themselves on the provided Graphics context.
//
// This may seem very complicated, but it makes the coordination of
// all the various different kinds of user input much easier.  For
// example here is how control flow works when the user presses the
// mouse button:
//
// (1) We set up an eventListener (actually we just use the same
//     eventListener that is being used by the timer.)
// (2) We register the eventListener with the window using the
//     addMouseListener() method.
// (3) Every time the mouse button is pressed inside the window the
//     window calls eventListener.mouseClicked().
// (4) eventListener.mouseClicked() modifies the logical
//     datastructures.  (In this example it calls ball.randomBump(), but
//     in other programs it might do something else, including request a
//     repaint operation).
//
public class AnimationWindow extends JPanel {
    // overview: an AnimationWindow is an area on the screen in which a
    // bouncing ball animation occurs.  AnimationWindows have two modes:
    // on and off.  During the on mode the ball moves, during the off
    // mode the ball doesn't move.

    private AnimationEventListener eventListener;
    public BouncingBall ball;
    protected Cezmi player1;
    protected Cezmi player2;

    protected ArrayList<Gizmo> gizmos = new ArrayList<Gizmo>();

    private Timer timer;
    protected Barrier barrier;
    private int mode;

    public AnimationWindow() {

        super();
        ball = new BouncingBall(this);
        eventListener = new AnimationEventListener();
        timer = new Timer(25, eventListener); // 40 fps
        mode = 0;
        this.player1 = new Cezmi(Wall.width / 4);
        this.player2 = new Cezmi(Wall.width / 4 * 3);

        barrier = new Barrier();
    }

    public Gizmo getGizmoFromPoint(Point p) {
        for(Gizmo gizmo : gizmos) {
            Rectangle rect = new Rectangle(gizmo.x, gizmo.y, 20, 20);
            if(rect.contains(p))
                return gizmo;
        }
        return null;
    }

    public int getGizmoCount(boolean area) {
        int i = 0;
        for(Gizmo gizmo : gizmos) {
            if((gizmo.x<200 && !area) || (gizmo.x>=200 && area)) i += 1;
        }
        return i;
    }

    public void paint(Graphics g) {
        super.paint(g);
        if (mode != 2) {
            ball.ball.paint(g);

            player1.paint(g);
            player2.paint(g);

            g.setColor(Color.BLACK);
            g.setFont(new Font("Arial", Font.BOLD, 16));
            g.drawString(Integer.toString(player1.score), 20, 20);
            g.drawString(Integer.toString(player2.score), Wall.width - 20, 20);

            if (mode==1)
                ball.move(g);
        }else {
            g.setColor(Color.RED);
            g.drawLine(200, 0, 200, 400);
            g.drawLine(0, 400, Wall.width, Wall.width);
        }
        for(Gizmo takoz : gizmos)
            takoz.paint(g);
        barrier.render(g);
    }

    void runTokat(int player, int idx) {
        int id = 0;
        for(Gizmo gizmo : gizmos) {
            if(gizmo instanceof Tokat)  {
                if ((gizmo.x < Wall.width/2 && player==0) || (gizmo.x > Wall.width/2 && player==1)) id += 1;
                if (id == idx) {
                    ((Tokat) gizmo).toggle();
                    return;
                }
            }
        }
    }

    public void setMode(int m) {
        // modifies: this
        // effects: changes the mode to <m>.
        if (mode == 0 || mode == 1) {
            // we're about to change mode: turn off all the old listeners
            removeMouseListener(eventListener);
            removeMouseMotionListener(eventListener);
            removeKeyListener(eventListener);
        }
        mode = m;
        if (mode == 1) {
            // the mode is true: turn on the listeners
            addMouseListener(eventListener);
            addMouseMotionListener(eventListener);
            addKeyListener(eventListener);
            requestFocus();           // make sure keyboard is directed to us
            timer.start();
        } else {
            timer.stop();
            if (mode == 0) {
                addKeyListener(new WaitEventListender());
            }
        }

    }

    class WaitEventListender extends MouseAdapter implements KeyListener {
        @Override
        public void keyTyped(KeyEvent keyEvent) {

        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {
            setMode(1);
        }

        @Override
        public void keyReleased(KeyEvent keyEvent) {

        }
    }

    class AnimationEventListener extends MouseAdapter implements KeyListener, ActionListener {


        // Here's the KeyListener interface
        public void keyPressed(KeyEvent e) {
            int keynum = e.getKeyCode();

            if (KeyEvent.VK_RIGHT == keynum) {
                if (player1.x + player1.radius < barrier.x)
                    player1.incrementX();
            } else if (KeyEvent.VK_LEFT == keynum) {
                if (player1.x - player1.radius> 0)
                    player1.decrementX();
            } else if (KeyEvent.VK_A == keynum) {
                if (player2.x + player2.radius > barrier.x)
                    player2.decrementX();
            } else if (KeyEvent.VK_S == keynum) {
                if (player2.x + player2.radius < Wall.width)
                    player2.incrementX();
            }else if (KeyEvent.VK_1 == keynum) {
                runTokat(0, 1);
            }else if (KeyEvent.VK_2 == keynum) {
                runTokat(0, 2);
            }else if (KeyEvent.VK_3 == keynum) {
                runTokat(0, 3);
            }else if (KeyEvent.VK_4 == keynum) {
                runTokat(0, 4);
            }else if (KeyEvent.VK_Q == keynum) {
                runTokat(1, 1);
            }else if (KeyEvent.VK_W == keynum) {
                runTokat(1, 2);
            }else if (KeyEvent.VK_E == keynum) {
                runTokat(1, 3);
            }else if (KeyEvent.VK_R == keynum) {
                runTokat(1, 4);
            }
        }

        public void keyReleased(KeyEvent e) {
        }

        public void keyTyped(KeyEvent e) {
        }

        // this is the callback for the timer
        public void actionPerformed(ActionEvent e) {

            repaint();
        }

    }
}
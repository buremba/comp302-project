import physics.LineSegment;
import physics.Vect;

import javax.swing.*;
import java.awt.*;

public class BouncingBall {

    private final AnimationWindow frame;
    // Overview: A BouncingBall is a mutable data type.  It simulates a
    // rubber ball bouncing inside a two dimensional box.  It also
    // provides methods that are useful for creating animations of the
    // ball as it moves.
    public Ball ball;

    public BouncingBall(AnimationWindow animationWindow) {
        frame = animationWindow;
    }

    public void move(Graphics g) {
        LineSegment pos = Wall.collide(ball);
        if (pos!=null && pos==Wall.bottom) {
            if(ball.x+ball.radius<frame.barrier.x)
                frame.player2.score += 1;
            else
                frame.player1.score += 1;
            if (frame.player1.score >= 4) {
                JOptionPane.showMessageDialog(null, "Player1 won!");
                frame.player2.score = frame.player1.score = 0;
            }else
            if (frame.player2.score >= 4) {
                JOptionPane.showMessageDialog(null, "Player2 won!");
                frame.player2.score = frame.player1.score = 0;
            }
            ball.setInitial();
            frame.setMode(0);
            frame.paint(g);
        }

        for(Gizmo takoz : frame.gizmos) {
            takoz.collide(ball);
        }

        frame.barrier.collide(ball);

        frame.player1.collide(ball);
        frame.player2.collide(ball);

        ball.setPosition(ball.getPosition().plus(ball.v));
        for(Gizmo gizmo : frame.gizmos) {
            if(gizmo instanceof Tokat)
                ((Tokat) gizmo).clock();
        }
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }


}
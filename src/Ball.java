import physics.Angle;
import physics.Circle;
import physics.Vect;

import java.awt.*;

public class Ball {
    final Color color = Color.RED;
    int x = 350;
    int y = 350;
    public Vect v;
    final int radius = 5;
    public float vx;
    public float vy;

    public Ball(double velocity) {
        v = new Vect(new Angle(.5, -1.0), 3);
    }

    public void setPosition(Vect z) {
        this.x = (int) z.x();
        this.y = (int) z.y();
    }

    public Vect getPosition() {
        return new Vect(x, y);
    }

    public Vect getPositionWithLength() {
        return new Vect(new Angle(x, y), radius);
    }

    public Circle getCircle() {
        return new Circle(x, y, radius);
    }

    public void setInitial() {
        this.x = Wall.width/2;
        this.y = Wall.px/2;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }

    /*
    public Ball() {
        this.vx = (float) (3f + (Math.random()*20f));
        this.vy = (float) (3f + (Math.random()*20f));
    } */
}
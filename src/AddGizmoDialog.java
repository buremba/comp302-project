import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class AddGizmoDialog extends JDialog {
    JLabel x = new JLabel("X");

    JLabel y = new JLabel("Y");

    JButton createButton = new JButton("Create");


    JTextField xInput = new JTextField();

    JTextField yInput = new JTextField();

    JComboBox gizmo = new JComboBox(new String[]{ "Square Takoz", "Triangle Takoz", "Tokat" });

    public AddGizmoDialog(Frame owner, boolean modal, AnimationWindow animationWindow) {
        super(owner, modal);
        init(animationWindow, owner);
    }
    private void init(final AnimationWindow animationWindow, final Frame owner) {
        this.setTitle("Add Gizmo");
        this.setLayout(new GridLayout(4, 2));
        this.add(x);
        this.add(xInput);
        this.add(y);
        this.add(yInput);
        this.add(gizmo);
        createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int x;
                int y;
                try {
                    x = Integer.parseInt(xInput.getText());
                    y = Integer.parseInt(xInput.getText());
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, "X and Y must be integers.");
                    return;
                }

                if (x<=20 && x>=0 && y <= 20 && y>=0) {
                    if(animationWindow.getGizmoFromPoint(new Point((x*20)+10, (y*20)+10))!=null) {
                        JOptionPane.showMessageDialog(null, "There is already a gizmo in that position");
                        return;
                    }
                    if(animationWindow.getGizmoCount(x>=10)>=4) {
                        JOptionPane.showMessageDialog(null, "Reached maximum gizmo number for that player.");
                        return;
                    }
                    String name = (String) gizmo.getSelectedItem();
                    if (name.equals("Square Takoz"))
                        animationWindow.gizmos.add(new SquareTakoz(x, y));
                    else if (name.equals("Tokat"))
                        animationWindow.gizmos.add(new Tokat(x, y));
                    else if (name.equals("Triangle Takoz"))
                        animationWindow.gizmos.add(new TriangleTakoz(x, y));
                    else
                        JOptionPane.showMessageDialog(null, "something is wrong");

                    owner.repaint();
                    closeDialog();
                } else
                    JOptionPane.showMessageDialog(null, "X and Y must be between 0 and 20.");
            }
        });
        this.add(createButton);
    }
    void closeDialog(){
        setModal(false);
        this.dispose();
    }
}
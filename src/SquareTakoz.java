import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

import java.awt.*;

/**
 * Created by buremba on 05/01/14.
 */
public class SquareTakoz extends Gizmo {
    final private LineSegment top;
    final private LineSegment right;
    final private LineSegment bottom;
    final private LineSegment left;

    public SquareTakoz(int block_x, int block_y) {
        super(Color.DARK_GRAY, block_x, block_y);

        top = new LineSegment(x, y, x + width, y);
        right = new LineSegment(x + width, y, x + width, y + height);
        bottom = new LineSegment(x, y + height, x + width, y + height);
        left = new LineSegment(x, y, x, y + height);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(color);
        g.fillRect(x, y, width,height);
    }

    @Override
    public void collide(Ball ball) {
        Circle c = ball.getCircle();
        Vect v = ball.v;
        if (Geometry.timeUntilWallCollision(top, c, v) <= 0)
            ball.v = Geometry.reflectWall(top, v);
        else if (Geometry.timeUntilWallCollision(right, c, v) <= 0)
            ball.v = Geometry.reflectWall(right, v);
        else if (Geometry.timeUntilWallCollision(bottom, c, v) <= 0)
            ball.v = Geometry.reflectWall(bottom, v);
        else if (Geometry.timeUntilWallCollision(left, c, v) <= 0)
            ball.v = Geometry.reflectWall(left, v);

    }
}

import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

import java.awt.*;

/**
 * Created by buremba on 05/01/14.
 */
public class Barrier {
    final static int width = 6;
    final static int height = 50;

    int x;
    int y;

    final LineSegment top;
    final LineSegment right;
    final LineSegment bottom;
    final LineSegment left;

    public Barrier() {
        this.x = (Wall.width / 2) - width / 2;
        this.y = Wall.px - height;

        top = new LineSegment(x, y, x+width, y);
        right = new LineSegment(x+width, y, x+width, y+height);
        bottom = new LineSegment(x, y+height, x+width, y+height);
        left = new LineSegment(x, y, x, y+height);
    }

    public void render(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(x, y, width,height);
    }

    public void collide(Ball ball) {
        Circle c = ball.getCircle();
        Vect v = ball.v;
        if(Geometry.timeUntilWallCollision(top, c, v)<=0)
            ball.v = Geometry.reflectWall(top, v);
        else if (Geometry.timeUntilWallCollision(right, c, v)<=0)
            ball.v = Geometry.reflectWall(right, v);
        else if (Geometry.timeUntilWallCollision(bottom, c, v)<=0)
            ball.v = Geometry.reflectWall(bottom, v);
        else if (Geometry.timeUntilWallCollision(left, c, v)<=0)
            ball.v = Geometry.reflectWall(left, v);
    }
}

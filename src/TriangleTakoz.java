import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

import java.awt.*;

/**
 * Created by buremba on 06/01/14.
 */
public class TriangleTakoz extends Gizmo {
    final private LineSegment top;
    final private LineSegment mop;
    final private LineSegment hop;
    private final boolean side;


    public TriangleTakoz(int block_x, int block_y) {
        super(Color.CYAN, block_x, block_y);

        side = block_x>10;

        if(side) {
            top = new LineSegment(x, y, x + width, y);
            mop = new LineSegment(x + width, y, x + width, y + height);
            hop = new LineSegment(x, y, x + width, y + height);
        }else {
            top = new LineSegment(x, y, x + width, y);
            mop = new LineSegment(x + width, y, x, y + height);
            hop = new LineSegment(x, y+height, x, y);
        }

    }

    @Override
    public void paint(Graphics g) {
        g.setColor(color);
        g.fillPolygon(new int[] {(int) top.p1().x(), (int) top.p2().x(), (int) mop.p2().x()}, new int[] {(int) top.p1().y(), (int) top.p2().y(), (int) mop.p2().y()}, 3);
    }

    @Override
    public void collide(Ball ball) {
        Circle c = ball.getCircle();
        Vect v = ball.v;
        if (Geometry.timeUntilWallCollision(top, c, v) <= 0)
            ball.v = Geometry.reflectWall(top, v);
        else if (Geometry.timeUntilWallCollision(mop, c, v) <= 0)
            ball.v = Geometry.reflectWall(mop, v);
        else if (Geometry.timeUntilWallCollision(hop, c, v) <= 0)
            ball.v = Geometry.reflectWall(hop, v);
    }
}

import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

import java.awt.*;

abstract class Gizmo {
    Color color;
    int x;
    int y;
    int height = 20;
    int width = 20;


    public Gizmo(Color color, int block_x, int block_y) {
        this.color = color;

        this.x = block_x*width;
        this.y = block_y*height;
    }

    public abstract void paint(Graphics g);
    public abstract void collide(Ball ball);
}
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

class SaveScenetoXMLDialog extends JDialog {
    JLabel filename = new JLabel("Filename");
    JButton createButton = new JButton("Create");
    JTextField filenameInput = new JTextField();

    public SaveScenetoXMLDialog(Frame owner, boolean modal, AnimationWindow animationWindow) {
        super(owner, modal);
        init(animationWindow, owner);
    }
    private void init(final AnimationWindow animationWindow, final Frame owner) {
        this.setTitle("Add Gizmo");
        this.setLayout(new GridLayout(4, 2));
        this.add(filename);
        this.add(filenameInput);
        createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = null;
                try {
                    docBuilder = docFactory.newDocumentBuilder();
                } catch (ParserConfigurationException e1) {
                    e1.printStackTrace();
                }

                Document doc = docBuilder.newDocument();
                Element rootElement = doc.createElement("board");
                doc.appendChild(rootElement);


                Element ball = doc.createElement("ball");
                rootElement.appendChild(ball);

                Element cezmi = doc.createElement("cezmi");
                rootElement.appendChild(cezmi);


                Element gizmos = doc.createElement("gizmos");
                rootElement.appendChild(gizmos);
                /*
                for (Gizmo gizmo : animationWindow.ball.gizmos) {
                    Element squareBumper = doc.createElement("squareBumper");

                    Attr xattr = doc.createAttribute("x");
                    xattr.setValue(String.valueOf(gizmo.x/20));
                    squareBumper.setAttributeNode(xattr);

                    Attr yattr = doc.createAttribute("y");
                    yattr.setValue(String.valueOf(gizmo.y/20));
                    squareBumper.setAttributeNode(yattr);
                    gizmos.appendChild(squareBumper);

                }*/


                Attr ballattrx = doc.createAttribute("x");
                ballattrx.setValue(String.valueOf(animationWindow.ball.ball.x - animationWindow.ball.ball.radius));
                ball.setAttributeNode(ballattrx);

                Attr ballattry = doc.createAttribute("y");
                ballattry.setValue(String.valueOf(animationWindow.ball.ball.x - animationWindow.ball.ball.radius));
                ball.setAttributeNode(ballattry);

                Attr ballattrvx = doc.createAttribute("xVelocity");
                ballattrvx.setValue(String.valueOf(animationWindow.ball.ball.vx));
                ball.setAttributeNode(ballattrvx);

                Attr ballattrvy = doc.createAttribute("yVelocity");
                ballattrvy.setValue(String.valueOf(animationWindow.ball.ball.vy));
                ball.setAttributeNode(ballattrvy);

                /*
                Attr cezmix = doc.createAttribute("x");
                cezmix.setValue(String.valueOf(animationWindow.player1.x));
                cezmi.setAttributeNode(cezmix);
                */
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = null;
                try {
                    transformer = transformerFactory.newTransformer();
                } catch (TransformerConfigurationException e1) {
                    e1.printStackTrace();
                }
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(filenameInput.getText()+".xml"));

                try {
                    transformer.transform(source, result);
                    closeDialog();
                } catch (TransformerException e1) {
                    JOptionPane.showMessageDialog(null, "The file couldn't saved.");
                }

            }
        });
        this.add(createButton);
    }
    void closeDialog(){
        setModal(false);
        this.dispose();
    }
}